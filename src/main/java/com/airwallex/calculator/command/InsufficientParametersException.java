package com.airwallex.calculator.command;

import com.google.common.collect.ImmutableMap;
import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.commons.text.StrSubstitutor;

/**
 * An exception representing an attempt to perform an operation on a stack with an insufficient amount of parameters
 * available to execute against.
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class InsufficientParametersException extends RuntimeException {
  private static String generateMessage(ParserRuleContext context) {
    return StrSubstitutor.replace(
        "operator ${operator} (position: ${position}): insufficient parameters",
        ImmutableMap.of(
            "operator", context.getRuleContext().getText(),
            "position", context.getStop().getCharPositionInLine() + 1 // increment position as the char position matching starts at index 0, requirements assume 1
        )
    );
  }

  public InsufficientParametersException(ParserRuleContext context) {
    super(generateMessage(context));
  }

  public InsufficientParametersException(ParserRuleContext context, Throwable cause) {
    super(generateMessage(context), cause);
  }
}
