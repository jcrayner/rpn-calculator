package com.airwallex.calculator.command;

import com.airwallex.calculator.event.StackMutationEvent;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Optional;
import java.util.Stack;

/**
 * Provides all commands required of an RPN Calculator
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class Commands {
  private static final Logger LOGGER = LoggerFactory.getLogger(Commands.class);
  private static final MathContext CALCULATION_CONTEXT = new MathContext(16, RoundingMode.FLOOR);
  
  // command singletons
  private static StackMutatorCommand<BigDecimal> ADD         = new AdditionCommand();
  private static StackMutatorCommand<BigDecimal> SUBTRACT    = new SubtractionCommand();
  private static StackMutatorCommand<BigDecimal> MULTIPLY    = new MultiplicationCommand();
  private static StackMutatorCommand<BigDecimal> DIVIDE      = new DivisionCommand();
  private static StackMutatorCommand<BigDecimal> SQUARE_ROOT = new SquareRootCommand();
  private static StackMutatorCommand<BigDecimal> CLEAR       = new ClearCommand();

  private Commands() { }

  // implementations

  /**
   * Stack mutator command interface.
   *
   * @param <Element> The element type of the Stack
   */
  public interface StackMutatorCommand<Element> {
    /**
     * Execute a mutation operation against the passed {@code Stack} and emit an optional {@code StackMutationEvent}
     * which can be utilised as a mechanism for either replaying the command or context for undoing to result of a
     * mutation.
     *
     * @param stack  A stack of elements
     * @return       An optional {@code StackMutationEvent}
     */
    Optional<StackMutationEvent<Element>> execute(Stack<Element> stack);
  }

  /**
   * A base abstract implementation of a stack mutator command that requires two items to be popped from the stack, then
   * apply a specific calculation implementation to them, resulting in a new item to be pushed onto the stack.
   *
   */
  private abstract static class ArithmeticCommand implements StackMutatorCommand<BigDecimal> {
    /**
     * Calculate a new value derived from the passed two values
     *
     * @param a The first value
     * @param b The second value
     * @return  A new value
     */
    abstract BigDecimal calculate(BigDecimal a, BigDecimal b);

    /**
     * The type of operation to be performed. This is used to manufacture an appropriate {@code StackMutationEvent} upon
     * completion.
     *
     * @return An operator type
     */
    abstract OperationType getOperatorType();

    @Override
    public Optional<StackMutationEvent<BigDecimal>> execute(Stack<BigDecimal> stack) {
      // need to check if the operation can be completed before attempting, as we
      // don't want to modify the stack unnecessarily, will throw an IllegalStateException if boolean is false
      Preconditions.checkState(stack.size() >= 2);

      final BigDecimal a       = stack.pop();
      final BigDecimal b       = stack.pop();
      final OperationType type = getOperatorType();

      LOGGER.debug("Executing operation {} {} {}", b, type.getSymbol(), a);

      stack.push(calculate(a, b));
      return Optional.of(new StackMutationEvent<>(type, a, b));
    }
  }

  /**
   * An arithmetic command for multiplying two numbers at an appropriate level of precision.
   */
  private static class MultiplicationCommand extends ArithmeticCommand {
    @Override
    BigDecimal calculate(BigDecimal a, BigDecimal b) {
      return b.multiply(a, CALCULATION_CONTEXT);
    }

    @Override
    OperationType getOperatorType() {
      return OperationType.MULTIPLICATION;
    }
  }

  /**
   * An arithmetic command for dividing two numbers at an appropriate level of precision.
   */
  private static class DivisionCommand extends ArithmeticCommand {
    @Override
    BigDecimal calculate(BigDecimal a, BigDecimal b) {
      return b.divide(a, CALCULATION_CONTEXT);
    }

    @Override
    OperationType getOperatorType() {
      return OperationType.DIVISION;
    }
  }

  /**
   * An arithmetic command for adding two numbers at an appropriate level of precision.
   */
  private static class AdditionCommand extends ArithmeticCommand {
    @Override
    BigDecimal calculate(BigDecimal a, BigDecimal b) {
      return b.add(a, CALCULATION_CONTEXT);
    }

    @Override
    OperationType getOperatorType() {
      return OperationType.ADDITION;
    }
  }

  /**
   * An arithmetic command for subtracting two numbers at an appropriate level of precision.
   */
  private static class SubtractionCommand extends ArithmeticCommand {
    @Override
    BigDecimal calculate(BigDecimal a, BigDecimal b) {
      return b.subtract(a, CALCULATION_CONTEXT);
    }

    @Override
    OperationType getOperatorType() {
      return OperationType.SUBTRACTION;
    }
  }

  /**
   * A command for calculating the square root of a number
   */
  private static class SquareRootCommand implements StackMutatorCommand<BigDecimal> {
    @Override
    public Optional<StackMutationEvent<BigDecimal>> execute(Stack<BigDecimal> stack) {
      // need to check if the operation can be completed before attempting, as we
      // don't want to modify the stack unnecessarily, will throw an IllegalStateException if boolean is false
      Preconditions.checkState(stack.size() >= 1);

      final BigDecimal value   = stack.pop();
      final OperationType type = OperationType.SQUARE_ROOT;

      LOGGER.debug("Executing operation {}{}", type.getSymbol(), value);

      stack.push(new BigDecimal(Math.sqrt(value.doubleValue()), CALCULATION_CONTEXT));
      return Optional.of(new StackMutationEvent<>(type, value));
    }
  }

  /**
   * A command for clearing the current contents of the passed {@code Stack}
   */
  private static class ClearCommand implements StackMutatorCommand<BigDecimal> {
    @Override
    public Optional<StackMutationEvent<BigDecimal>> execute(Stack<BigDecimal> stack) {
      LOGGER.debug("Clearing stack");
      stack.clear();
      return Optional.empty();
    }
  }

  /**
   * A command for pushing a new item on to the passed {@code Stack}
   */
  private static class PushCommand implements StackMutatorCommand<BigDecimal> {
    private final BigDecimal value;

    PushCommand(BigDecimal value) {
      this.value = value;
    }

    @Override
    public Optional<StackMutationEvent<BigDecimal>> execute(Stack<BigDecimal> stack) {
      final OperationType type = OperationType.PUSH;
      LOGGER.debug("Executing command {} {}", type.getSymbol(), value);
      stack.push(value.round(CALCULATION_CONTEXT));
      return Optional.of(new StackMutationEvent<>(type, value));
    }
  }

  /**
   * A command implementation for undoing the last mutation operation performed against a
   * {@code Stack}.
   */
  private static class UndoCommand implements StackMutatorCommand<BigDecimal> {
    private Stack<StackMutationEvent<BigDecimal>> history;

    UndoCommand(Stack<StackMutationEvent<BigDecimal>> history) {
      this.history = history;
    }

    @Override
    public Optional<StackMutationEvent<BigDecimal>> execute(Stack<BigDecimal> stack) {
      // need to check if the operation can be completed before attempting, as we
      // don't want to modify the history stack unnecessarily, will throw an IllegalStateException if boolean is false
      Preconditions.checkState(history.size() >= 1);

      final StackMutationEvent<BigDecimal> lastEvent = history.pop();
      final OperationType type = lastEvent.getOperationType();

      LOGGER.debug("Undoing previous operation {}", lastEvent);

      switch(type) {
        case PUSH:
          final Collection<BigDecimal> values = lastEvent.getValues();
          Preconditions.checkArgument(
              // this will
              values.size() == 1 && values.stream().allMatch(value -> value.equals(stack.peek())),
              "Top value of the stack is not the same as the last mutation event value"
          );
          stack.pop(); // simply remove the last element on the stack
          break;

        case ADDITION:
        case SUBTRACTION:
        case DIVISION:
        case MULTIPLICATION:
        case SQUARE_ROOT:
          stack.pop(); // remove the last element on the stack
          stack.addAll(lastEvent.getValues()); // reconstitute the previous values
          break;

        default:
          throw new IllegalArgumentException("Unsupported operation type " + type + " provided");
      }

      return Optional.empty();
    }
  }

  // command accessors
  
  public static StackMutatorCommand<BigDecimal> add() {
    return ADD;
  }

  public static StackMutatorCommand<BigDecimal> subtract() {
    return SUBTRACT;
  }

  public static StackMutatorCommand<BigDecimal> multiply() {
    return MULTIPLY;
  }

  public static StackMutatorCommand<BigDecimal> divide() {
    return DIVIDE;
  }

  public static StackMutatorCommand<BigDecimal> squareRoot() {
    return SQUARE_ROOT;
  }

  public static StackMutatorCommand<BigDecimal> clear() {
    return CLEAR;
  }

  public static StackMutatorCommand<BigDecimal> push(final BigDecimal value) {
    return new PushCommand(value);
  }

  public static StackMutatorCommand<BigDecimal> undo(Stack<StackMutationEvent<BigDecimal>> history) {
    return new UndoCommand(history);
  }
}
