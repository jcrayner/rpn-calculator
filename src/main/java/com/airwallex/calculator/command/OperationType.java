package com.airwallex.calculator.command;

import com.google.common.base.MoreObjects;

/**
 * Types for command operations
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public enum OperationType {
  ADDITION("+"),
  SUBTRACTION("-"),
  DIVISION("/"),
  MULTIPLICATION("*"),
  SQUARE_ROOT("√"),
  PUSH(">>");

  private final String symbol;

  OperationType(String symbol) {
    this.symbol = symbol;
  }

  public String getSymbol() {
    return symbol;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("symbol", symbol)
        .toString();
  }
}
