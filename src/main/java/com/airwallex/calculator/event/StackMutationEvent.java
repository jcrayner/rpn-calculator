package com.airwallex.calculator.event;

import com.airwallex.calculator.command.OperationType;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * An event representing the outcome of an operation on a {@code Stack}
 * @param <Element> An element type associated with the {@code Stack}
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class StackMutationEvent<Element> {
  private final OperationType operationType;
  private final Collection<Element> values;

  public StackMutationEvent(final OperationType operationType) {
    this(operationType, Collections.emptyList());
  }

  public StackMutationEvent(final OperationType operationType, Element value) {
    this(operationType, Collections.singletonList(value));
  }

  public StackMutationEvent(final OperationType operationType, Element value1, Element value2) {
    this(operationType, ImmutableList.of(value2, value1));
  }

  private StackMutationEvent(final OperationType operationType, Collection<Element> values) {
    this.operationType = operationType;
    this.values = values;
  }

  public Collection<Element> getValues() {
    return values;
  }

  public OperationType getOperationType() {
    return operationType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    StackMutationEvent<?> that = (StackMutationEvent<?>) o;
    return operationType == that.operationType &&
        Objects.equals(values, that.values);
  }

  @Override
  public int hashCode() {
    return Objects.hash(operationType, values);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("operationType", operationType)
        .add("values", values)
        .toString();
  }
}
