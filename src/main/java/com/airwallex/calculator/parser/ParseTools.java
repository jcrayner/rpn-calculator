package com.airwallex.calculator.parser;

import com.airwallex.calculator.parser.generated.CalculationLexer;
import com.airwallex.calculator.parser.generated.CalculationParser;
import com.google.common.base.Throwables;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.LexerNoViableAltException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * Basic tools for parsing strings into a {@code ParseTree}.
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class ParseTools {
  private static final Logger LOGGER = LoggerFactory.getLogger(ParseTools.class);

  /**
   * Generate an AST from the passed {@code String}
   *
   * @param line   A string to be parsed
   * @return       A {@code ParseTree}
   */
  public static ParseTree getTree(final String line) {
    CodePointCharStream input = CharStreams.fromString(line);
    ParserBailOutLexer lexer  = new ParserBailOutLexer(input);
    CommonTokenStream tokens  = new CommonTokenStream(lexer);
    CalculationParser parser  = new CalculationParser(tokens);

    parser.removeErrorListeners();
    parser.addErrorListener(new ParserErrorListener());
    parser.setErrorHandler(new BailErrorStrategy());

    return parser.calculation();
  }

  public static class ParserBailOutLexer extends CalculationLexer {
    ParserBailOutLexer(CharStream input) {
      super(input);
    }

    public void recover(LexerNoViableAltException e) {
      Throwables.throwIfUnchecked(e);
      throw new RuntimeException(e); // Bail out
    }
  }

  /** Excerpted from "The Definitive ANTLR 4 Reference" * */
  public static class ParserErrorListener extends BaseErrorListener {
    int lastError = -1;

    @Override
    public void syntaxError(
        Recognizer<?, ?> recognizer,
        Object offendingSymbol,
        int line,
        int charPositionInLine,
        String msg,
        RecognitionException e) {

      Parser parser = (Parser) recognizer;
      TokenStream tokens = parser.getInputStream();

      Token offSymbol = (Token) offendingSymbol;
      int thisError = offSymbol.getTokenIndex();

      if (offSymbol.getType() == -1 && thisError == tokens.size() - 1) {
        LOGGER.error("Incorrect error: " + msg);
        return;
      }

      if (thisError > lastError + 20) {
        lastError = thisError - 20;
      }

      for (int idx = lastError + 1; idx <= thisError; idx++) {
        Token token = tokens.get(idx);

        if (token.getChannel() != Token.HIDDEN_CHANNEL) {
          LOGGER.error(token.toString());
        }
      }

      lastError = thisError;

      List<String> stack = parser.getRuleInvocationStack();
      Collections.reverse(stack);

      LOGGER.error("Rule stack: {}", stack);
      LOGGER.error("Line {}: {} at {}: {}", line, charPositionInLine, offendingSymbol, msg);
    }
  }
}
