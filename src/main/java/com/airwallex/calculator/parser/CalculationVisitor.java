package com.airwallex.calculator.parser;

import com.airwallex.calculator.command.Commands;
import com.airwallex.calculator.command.InsufficientParametersException;
import com.airwallex.calculator.event.StackMutationEvent;
import com.airwallex.calculator.parser.generated.CalculationBaseVisitor;
import com.airwallex.calculator.parser.generated.CalculationParser;

import java.math.BigDecimal;
import java.util.Stack;
import java.util.function.Consumer;

/**
 * A calculation {@code ParseTree} visitor implementation. Upon visiting a node, execute a command relevant to the type of
 * node being visited.
 *
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class CalculationVisitor extends CalculationBaseVisitor<Void> {
  private Stack<BigDecimal> stack;
  private Stack<StackMutationEvent<BigDecimal>> events;

  public CalculationVisitor(Stack<BigDecimal> stack) {
    this.stack  = stack;
    this.events = new Stack<>();
  }

  @Override
  public Void visitOPERATION(CalculationParser.OPERATIONContext ctx) {
    try {
      return super.visitOPERATION(ctx);
    } catch(IllegalStateException e) {
      throw new InsufficientParametersException(ctx, e);
    }
  }

  @Override
  public Void visitVALUE(CalculationParser.VALUEContext ctx) {
    execute(Commands.push(new BigDecimal(ctx.getText())));
    return super.visitVALUE(ctx);
  }

  @Override
  public Void visitPLUS(CalculationParser.PLUSContext ctx) {
    execute(Commands.add());
    return super.visitPLUS(ctx);
  }

  @Override
  public Void visitMINUS(CalculationParser.MINUSContext ctx) {
    execute(Commands.subtract());
    return super.visitMINUS(ctx);
  }

  @Override
  public Void visitMULTIPLY(CalculationParser.MULTIPLYContext ctx) {
    execute(Commands.multiply());
    return super.visitMULTIPLY(ctx);
  }

  @Override
  public Void visitDIVIDE(CalculationParser.DIVIDEContext ctx) {
    execute(Commands.divide());
    return super.visitDIVIDE(ctx);
  }

  @Override
  public Void visitSQUARE_ROOT(CalculationParser.SQUARE_ROOTContext ctx) {
    execute(Commands.squareRoot());
    return super.visitSQUARE_ROOT(ctx);
  }

  @Override
  public Void visitUNDO(CalculationParser.UNDOContext ctx) {
    execute(Commands.undo(events));
    return super.visitUNDO(ctx);
  }

  @Override
  public Void visitCLEAR(CalculationParser.CLEARContext ctx) {
    execute(Commands.clear());
    return super.visitCLEAR(ctx);
  }

  /**
   * Execute the passed command and consume returned event (if present)
   *
   * @param command A stack mutator command
   */
  private void execute(Commands.StackMutatorCommand<BigDecimal> command) {
    command.execute(stack).ifPresent(events::push);
  }
}
