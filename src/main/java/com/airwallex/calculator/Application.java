package com.airwallex.calculator;

import com.airwallex.calculator.command.InsufficientParametersException;
import com.airwallex.calculator.parser.CalculationVisitor;
import com.airwallex.calculator.parser.ParseTools;
import com.google.common.collect.ImmutableMap;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Stack;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * RPN Calculator application
 * @author Jeremy Rayner <jeremy@davros.com.au>
 */
public class Application {
  private static Logger LOGGER = LoggerFactory.getLogger(Application.class);

  /**
   * Application entry point. Initialise the application and run.
   * @param args command line arguments
   */
  public static void main(String args[]) {
    final Application application = new Application();

    try(final InputStream input = System.in; final OutputStream output = System.out) {
      application.run(input, output);
    } catch(Throwable e) {
      LOGGER.error("An unrecoverable error has occurred whilst processing the calculator input", e);
      System.exit(1);
    }
  }

  /**
   * Run the RPN calculator
   *
   * @param input   An input stream for reading incoming commands
   * @param output  An output stream for writing stack updates and informational messaging
   *
   * @throws Exception  Any exception that cannot be handled by the main application logic
   */
  private void run(InputStream input, OutputStream output) throws Exception {
    final Stack<BigDecimal> stack    = new Stack<>();
    final CalculationVisitor visitor = new CalculationVisitor(stack);
    final BufferedReader reader      = new BufferedReader(new InputStreamReader(input));
    final BufferedWriter writer      = new BufferedWriter(new OutputStreamWriter(output));

    final MathContext context = new MathContext(11, RoundingMode.FLOOR);

    String line;

    // while we have input
    while((line = reader.readLine()) != null) {
      try {
        final ParseTree tree = ParseTools.getTree(line); // parse an AST from the command line input
        visitor.visit(tree); // visit ParseTree nodes and execute commands
      } catch(InsufficientParametersException e) {
        writer.write(e.getMessage());
        writer.newLine();

        LOGGER.warn(e.getMessage(), e);
      } catch(RecognitionException re) {
        writer.write(
            StrSubstitutor.replace(
                "Invalid character '${char}' detected in input, ignoring",
                ImmutableMap.of("char", re.getInputStream().toString())
            )
        );

        writer.newLine();
        LOGGER.warn(re.getMessage(), re);
      }

      // always write out the content of the stack
      writer.write(
          StrSubstitutor.replace(
              "Stack: ${values}",
              ImmutableMap.of(
                  "values",
                  stack.stream()
                      .map(((Function<BigDecimal, BigDecimal>) item -> item.round(context)).andThen(BigDecimal::toPlainString))
                      .collect(Collectors.joining(" "))
              )
          )
      );

      writer.newLine();
      writer.flush();
    }
  }
}
