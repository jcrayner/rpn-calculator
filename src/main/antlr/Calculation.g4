grammar Calculation;

calculation: expr? EOF ;

expr
  : input (input)*
  ;

input
  : NUMBER      # VALUE
  | operator    # OPERATION
  ;

operator
  : PLUS        # PLUS
  | MINUS       # MINUS
  | MULTIPLY    # MULTIPLY
  | DIVIDE      # DIVIDE
  | SQUARE_ROOT # SQUARE_ROOT
  | UNDO        # UNDO
  | CLEAR       # CLEAR
  ;

PLUS: '+' ;
MINUS: '-' ;
MULTIPLY: '*' ;
DIVIDE: '/' ;
SQUARE_ROOT: 'sqrt' ;
UNDO: 'undo' ;
CLEAR: 'clear' ;

NUMBER
  : '-'? INT '.' [0-9]+ EXP
  | '-'? INT '.' INT
  | '-'? INT EXP
  | '-'? INT
  ;

fragment INT: [0-9]+ ;
fragment EXP: [Ee] [+\-]? INT;

WS: [ \t\r\n]+ -> channel(HIDDEN) ;