package com.airwallex.calculator.command;

import com.airwallex.calculator.event.StackMutationEvent;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.collection.IsIterableWithSize;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.util.Stack;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class CommandsTest {
  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  @Test
  public void testAPushCommandThenUndoIt() throws Exception {
    Stack<BigDecimal> stack = new Stack<>();
    Stack<StackMutationEvent<BigDecimal>> events = new Stack<>();

    Commands.push(BigDecimal.ONE).execute(stack).ifPresent(events::push);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(1)),
            IsCollectionContaining.hasItem(equalTo(BigDecimal.ONE))
        )
    );

    Commands.undo(events).execute(stack);
    assertThat(stack, IsEmptyCollection.emptyCollectionOf(BigDecimal.class));
  }

  @Test
  public void testAnArithmeticCommandWithAnIncompleteStack() throws Exception {
    expectedException.expect(IllegalStateException.class);

    Stack<BigDecimal> stack = new Stack<>();
    Commands.push(BigDecimal.ONE).execute(stack);
    Commands.add().execute(stack);
  }

  @Test
  public void testAnAdditionCommandThenUndoIt() throws Exception {
    Stack<BigDecimal> stack = new Stack<>();
    Stack<StackMutationEvent<BigDecimal>> events = new Stack<>();

    Commands.push(BigDecimal.ONE).execute(stack).ifPresent(events::push);
    Commands.push(BigDecimal.ONE).execute(stack).ifPresent(events::push);
    Commands.add().execute(stack).ifPresent(events::push);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(1)),
            IsCollectionContaining.hasItem(equalTo(new BigDecimal(2)))
        )
    );

    Commands.undo(events).execute(stack);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(2)),
            IsCollectionContaining.hasItems(equalTo(BigDecimal.ONE), equalTo(BigDecimal.ONE))
        )
    );
  }

  @Test
  public void testASubtractionCommandThenUndoIt() throws Exception {
    Stack<BigDecimal> stack = new Stack<>();
    Stack<StackMutationEvent<BigDecimal>> events = new Stack<>();

    Commands.push(BigDecimal.ONE).execute(stack).ifPresent(events::push);
    Commands.push(BigDecimal.ONE).execute(stack).ifPresent(events::push);
    Commands.subtract().execute(stack).ifPresent(events::push);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(1)),
            IsCollectionContaining.hasItem(equalTo(BigDecimal.ZERO))
        )
    );

    Commands.undo(events).execute(stack);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(2)),
            IsCollectionContaining.hasItems(equalTo(BigDecimal.ONE), equalTo(BigDecimal.ONE))
        )
    );
  }

  @Test
  public void testAMultiplicationCommandThenUndoIt() throws Exception {
    Stack<BigDecimal> stack = new Stack<>();
    Stack<StackMutationEvent<BigDecimal>> events = new Stack<>();

    Commands.push(BigDecimal.TEN).execute(stack).ifPresent(events::push);
    Commands.push(BigDecimal.TEN).execute(stack).ifPresent(events::push);
    Commands.multiply().execute(stack).ifPresent(events::push);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(1)),
            IsCollectionContaining.hasItem(equalTo(new BigDecimal(100)))
        )
    );

    Commands.undo(events).execute(stack);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(2)),
            IsCollectionContaining.hasItems(equalTo(BigDecimal.TEN), equalTo(BigDecimal.TEN))
        )
    );
  }

  @Test
  public void testADivisionCommandThenUndoIt() throws Exception {
    Stack<BigDecimal> stack = new Stack<>();
    Stack<StackMutationEvent<BigDecimal>> events = new Stack<>();

    Commands.push(BigDecimal.TEN).execute(stack).ifPresent(events::push);
    Commands.push(new BigDecimal(2)).execute(stack).ifPresent(events::push);
    Commands.divide().execute(stack).ifPresent(events::push);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(1)),
            IsCollectionContaining.hasItem(equalTo(new BigDecimal(5)))
        )
    );

    Commands.undo(events).execute(stack);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(2)),
            IsCollectionContaining.hasItems(equalTo(BigDecimal.TEN), equalTo(new BigDecimal(2)))
        )
    );
  }

  @Test
  public void testASquareRootCommandThenUndoIt() throws Exception {
    Stack<BigDecimal> stack = new Stack<>();
    Stack<StackMutationEvent<BigDecimal>> events = new Stack<>();

    Commands.push(new BigDecimal(4)).execute(stack).ifPresent(events::push);
    Commands.squareRoot().execute(stack).ifPresent(events::push);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(1)),
            IsCollectionContaining.hasItem(equalTo(new BigDecimal(2)))
        )
    );

    Commands.undo(events).execute(stack);
    assertThat(
        stack,
        Matchers.allOf(
            IsIterableWithSize.iterableWithSize(equalTo(1)),
            IsCollectionContaining.hasItem(equalTo(new BigDecimal(4)))
        )
    );
  }

  @Test
  public void testSquareRootCommandWithAnEmptyStack() throws Exception {
    expectedException.expect(IllegalStateException.class);

    Stack<BigDecimal> stack = new Stack<>();
    Commands.squareRoot().execute(stack);
  }

  @Test
  public void testClearCommandClearsTheStack() throws Exception {
    Stack<BigDecimal> stack = new Stack<>();

    Commands.push(BigDecimal.TEN).execute(stack);
    Commands.push(new BigDecimal(2)).execute(stack);
    Commands.clear().execute(stack);

    assertThat(stack, IsEmptyCollection.emptyCollectionOf(BigDecimal.class));
  }
}