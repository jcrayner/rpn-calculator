package com.airwallex.calculator;

import com.airwallex.calculator.categories.IntegrationTest;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Resources;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.core.Is.is;

@RunWith(Parameterized.class)
@Category(IntegrationTest.class)
public class ApplicationTest {
  @Parameterized.Parameters(name = "input: {0}, output: {1}")
  public static Collection<Object[]> fixtures() {
    ImmutableList.Builder<Object[]> builder = ImmutableList.<Object[]>builder();

    for(int i = 1; i < 9; i++) {
      builder.add(
          new Object[] {
              Resources.getResource("example" + i + "-input.txt"),
              Resources.getResource("example" + i + "-output.txt")
          }
      );
    }

    return builder.build();
  }

  private final URL inputFixture;
  private final URL expectedOutputFixture;

  public ApplicationTest(URL inputFixture, URL expectedOutputFixture) {
    this.inputFixture = inputFixture;
    this.expectedOutputFixture = expectedOutputFixture;
  }

  @Test
  public void testApplicationE2E() throws Exception {
    final String expected = Resources.toString(expectedOutputFixture, Charsets.UTF_8);

    // maintain OG STDIN/STDOUT
    final InputStream stdin  = System.in;
    final PrintStream stdout = System.out;

    try(final InputStream input = inputFixture.openStream()) {
      System.setIn(input);

      try(final OutputStream stream = new ByteArrayOutputStream()) {
        final PrintStream output = new PrintStream(stream, true, Charsets.UTF_8.displayName());
        System.setOut(output);

        Application.main(null);
        assertThat(stream.toString(), is(equalToIgnoringWhiteSpace(expected)));
      } finally {
        System.setOut(stdout); // reset STDOUT
      }
    } finally {
      System.setIn(stdin); // reset STDIN
    }
  }
}
