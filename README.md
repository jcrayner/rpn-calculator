#RPN Calculator
A simple RPN calculator implementation consisting of a few key parts

- An antlr grammar, parser and visitor implementations
- An application bootstrap for feeding stdin to the grammar parser and emitting stack state and error information to stdout
- A command / abstract factory implementation providing discrete stack mutation operations derived from application input

Requirements: Java 8

Tests can be run using the following:

    ./gradlew clean test
    
The application can be run using the following:

    ./gradlew run
    
An executable jar can be manufactured using the following:

    ./gradlew clean shadowJar
    
And then executed

    java -jar <path>/<to>/<jar>/rpn-calculator-1.0-SNAPSHOT-all.jar
    
